package au.net.causal.maven.plugins.dockerimagearchive;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TestPathTranslator
{
    private final PathTranslator pathTranslator = new PathTranslator();

    @Test
    public void lowerCaseDriveLetterTranslation()
    {
        String result = pathTranslator.translatePath("c:\\data\\MyFiles");
        assertThat(result, is("/c/data/MyFiles"));
    }

    @Test
    public void upperCaseDriveLetterTranslation()
    {
        String result = pathTranslator.translatePath("C:\\data\\MyFiles");
        assertThat(result, is("/c/data/MyFiles"));
    }

    @Test
    public void mixedSeparators()
    {
        String result = pathTranslator.translatePath("C:\\data/MyFiles\\something/else.txt");
        assertThat(result, is("/c/data/MyFiles/something/else.txt"));
    }

}
