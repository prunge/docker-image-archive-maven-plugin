package au.net.causal.maven.plugins.dockerimagearchive;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathTranslator
{
    //I've heard that you can do 0-9 as drive letters with subst so include just in case
    private static final Pattern STARTS_WITH_DRIVE_LETTER_PATTERN = Pattern.compile("^([A-Za-z0-9]):\\\\");

    public String translatePath(String path)
    {
        //Lowercase the drive letter
        //<letter>:\ -> <lowercased_letter>:\
        //and then convert this to /<lowercased_letter>/
        Matcher m = STARTS_WITH_DRIVE_LETTER_PATTERN.matcher(path);
        if (m.find())
        {
            String driveLetter = m.group(1);
            driveLetter = driveLetter.toLowerCase(Locale.ENGLISH);
            path = "/" + driveLetter + "/" + path.substring(m.end());
        }

        //Translate all remaining \ file separators to docker expected form /
        path = path.replace('\\', '/');

        return path;
    }
}
