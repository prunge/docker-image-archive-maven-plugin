package au.net.causal.maven.plugins.dockerimagearchive;

public class ImageArchive
{
    private String groupId;
    private String artifactId;
    private String version;
    private String type;
    private String classifier;

    public String getArtifactId()
    {
        return artifactId;
    }

    public void setArtifactId(String artifactId)
    {
        this.artifactId = artifactId;
    }

    public String getClassifier()
    {
        return classifier;
    }

    public void setClassifier(String classifier)
    {
        this.classifier = classifier;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("ImageArchive{");
        sb.append("artifactId='").append(artifactId).append('\'');
        sb.append(", groupId='").append(groupId).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", classifier='").append(classifier).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
