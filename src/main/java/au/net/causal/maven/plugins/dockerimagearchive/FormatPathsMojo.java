package au.net.causal.maven.plugins.dockerimagearchive;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.utils.Os;

/**
 * Defines new properties that fix Windows paths to be in docker format.
 */
@Mojo(name="format-paths", defaultPhase = LifecyclePhase.VALIDATE, threadSafe = true)
public class FormatPathsMojo extends AbstractMojo
{
    @Parameter(defaultValue="${project}", readonly=true, required=true)
    private MavenProject project;

    @Parameter(required = true)
    private Map<String, String> pathProperties;

    private final PathTranslator pathTranslator = new PathTranslator();

    /**
     * If true, translation of paths will occur even when not running on Windows.
     */
    @Parameter(defaultValue = "false")
    private boolean forceTranslation;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        Map<String, String> modifiablePathProperties = new HashMap<>(pathProperties);
        translateMap(modifiablePathProperties);
        getLog().debug("Defining properties: " + modifiablePathProperties);
        project.getProperties().putAll(modifiablePathProperties);
    }

    protected boolean isTranslationRequired()
    {
        return forceTranslation || Os.isFamily(Os.FAMILY_WINDOWS);
    }

    private void translateMap(Map<String, String> pathMap)
    {
        if (isTranslationRequired())
            pathMap.replaceAll((property, path) -> translatePath(path));
    }

    protected String translatePath(String path)
    {
        return pathTranslator.translatePath(path);
    }
}
