package au.net.causal.maven.plugins.dockerimagearchive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.maven.RepositoryUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.jolokia.docker.maven.AbstractDockerMojo;
import org.jolokia.docker.maven.access.DockerAccess;
import org.jolokia.docker.maven.access.hc.DockerAccessWithHcClient;
import org.jolokia.docker.maven.util.AnsiLogger;
import org.jolokia.docker.maven.util.EnvUtil;
import org.jolokia.docker.maven.util.Logger;

/**
 * Build a docker image from an archive.
 */
@Mojo(name="build")
public class BuildMojo extends AbstractMojo
{
    @Component
    private RepositorySystem repositorySystem;

    @Parameter(defaultValue="${project}", readonly=true, required=true)
    private MavenProject project;

    @Parameter(defaultValue="${repositorySystemSession}", readonly=true, required=true)
    private RepositorySystemSession repositorySession;

    @Parameter(required=true)
    private List<ImageArchive> imageArchives;

    //Basic docker configuration, copied from AbstractDockerMojo

    @Parameter(property = "docker.apiVersion")
    private String apiVersion;

    // URL to docker daemon
    @Parameter(property = "docker.host")
    private String dockerHost;

    @Parameter(property = "docker.certPath")
    private String certPath;

    // If logging is enabled globally

    // Whether to use color
    @Parameter(property = "docker.useColor", defaultValue = "true")
    private boolean useColor;

    // For verbose output
    @Parameter(property = "docker.verbose", defaultValue = "false")
    private boolean verbose;

    // maximum connection to use in parallel for connecting the docker host
    @Parameter(property = "docker.maxConnections", defaultValue = "100")
    private int maxConnections;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        List<ArtifactRepository> artifactRepos = new ArrayList<>();
        if (project.getDistributionManagementArtifactRepository() != null)
            artifactRepos.add(project.getDistributionManagementArtifactRepository());

        artifactRepos.addAll(project.getRemoteArtifactRepositories());

        List<RemoteRepository> repos = RepositoryUtils.toRepos(artifactRepos);
        List<ArtifactRequest> requests = imageArchives.stream()
                                            .map(imageArchive -> createArtifactRequest(imageArchive, repos))
                                            .collect(Collectors.toList());
        List<Artifact> artifacts;

        try
        {
            List<ArtifactResult> results = repositorySystem.resolveArtifacts(repositorySession, requests);
            getLog().info("Resolved results: " + results);
            getLog().info("Files: " + results.stream().map(ArtifactResult::getArtifact).map(Artifact::getFile).collect(Collectors.toList()));

            artifacts = results.stream().map(ArtifactResult::getArtifact).collect(Collectors.toList());
        }
        catch (ArtifactResolutionException e)
        {
            throw new MojoExecutionException("Failed to resolve artifacts: " + e.getMessage(), e);
        }

        //OK let's invoke the docker
        try
        {
            buildDockerImages(artifacts);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Docker failed to build images: " + e, e);
        }
    }

    private void buildDockerImages(Collection<? extends Artifact> artifacts)
    throws IOException
    {
        Logger log = new AnsiLogger(getLog(), useColor, verbose);
        String dockerUrl = EnvUtil.extractUrl(dockerHost);
        String version = (apiVersion == null) ? AbstractDockerMojo.API_VERSION : apiVersion;
        DockerAccess client = new DockerAccessWithHcClient(version, dockerUrl, EnvUtil.getCertPath(certPath), maxConnections, log);

        for (Artifact artifact : artifacts)
        {
            getLog().info("Building docker image: " + artifact.getGroupId() + ":" + artifact.getArtifactId() + ":" + artifact.getVersion());
            String name = artifact.getGroupId() + "/" + artifact.getArtifactId() + ":" + artifact.getVersion();
            boolean removeIntermediateContainers = true;
            boolean noCache = false;
            client.buildImage(name, artifact.getFile(), removeIntermediateContainers, noCache);
        }
    }

    private ArtifactRequest createArtifactRequest(ImageArchive imageArchive, List<RemoteRepository> repos)
    {
        ArtifactRequest request = new ArtifactRequest();
        request.setRepositories(repos);
        request.setArtifact(new DefaultArtifact(imageArchive.getGroupId(), imageArchive.getArtifactId(), imageArchive.getClassifier(), imageArchive.getType(), imageArchive.getVersion()));

        return request;
    }
}
