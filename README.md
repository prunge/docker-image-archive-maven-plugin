The Docker Image Archive Maven Plugin complements the [Docker Maven Plugin](https://github.com/rhuss/docker-maven-plugin)
by rhuss.

This plugin builds images from docker image archives, generated from the Docker Maven Plugin's `source` goal.
This is useful if you need to use a docker image whose definition is uploaded in a Maven repository.

Usage
-----

This is an example of using the image archive plugin to generate a docker image from a docker archive:

```xml
<plugin>
    <groupId>au.net.causal.maven.plugins</groupId>
    <artifactId>docker-image-archive-maven-plugin</artifactId>
    <version>1.0</version>
    <executions>
        <execution>
            <id>build-my-image</id>
            <phase>generate-sources</phase>
            <goals>
                <goal>build</goal>
            </goals>
            <configuration>
                <imageArchives>
                    <!-- Specify each image you need to build by imageArchive elements -->
                    <imageArchive>
                        <groupId>au.net.causal.crosscompile</groupId>
                        <artifactId>crosscompile-mac</artifactId>
                        <version>1.0-SNAPSHOT</version>
                        <type>tar.gz</type>
                        <classifier>docker-archive</classifier>
                    </imageArchive>
                </imageArchives>
            </configuration>
        </execution>
    </executions>
</plugin>
```

Each `imageArchive` element corresponds to a docker image archive built with the Docker Maven Plugin.

The image archive plugin will download each image archive from the Maven repository (or use the one in your local
repo if you have it already) and build a docker image out of it.  You can then use that docker image from then on
in your build through the standard Docker plugin.

Generating a Docker image archive
---------------------------------

Use the standard Docker plugin for that as described in
[its documentation](http://ro14nd.de/docker-maven-plugin/docker-source.html).

Why not just use the Docker plugin for building the image?
----------------------------------------------------------

Sometimes the configuration for creating a docker image can be complex.  It might involve a docker file and a custom
assembly descriptor.  Using this plugin can make using your docker images simpler.